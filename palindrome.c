#include <stdio.h>
#include <string.h>

int main(){
	
	char input1[25];
	char input2[25];
	
	puts("Enter a first string");
	scanf("%s", input1);
	
	strcpy(input2, input1);
	
	strrev(input2);
	
	if (strcmp(input1, input2) == 0){
		puts("strings are palindrome");
	}
	else {
		puts("strings are not palindrome");
	}
}
