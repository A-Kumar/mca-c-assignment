
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {

	char name[10];
	unsigned int roll_no;
	char semester[11];
	char course[11];
	char mobile_no[10];
	char address[20];
	char email[25];
	printf("\nPlease enter the name of the student: ");
	scanf("%s", &name);
	strupr(name);
	fflush(stdin);

	
	printf("\nPlease enter the roll no. of the student: ");
	scanf("%d", &roll_no);
	fflush(stdin);
	
	printf("\nPlease enter the semester of the student: ");
	scanf("%s", &semester);
	fflush(stdin);
	
	printf("\nPlease enter the course of the student: ");
	scanf("%s", &course);
	strupr(course);
	fflush(stdin);
	
	printf("\nPlease enter the mobile no. of the student: ");
	scanf("%s", &mobile_no);
	fflush(stdin);
	
	printf("\nPlease enter the address of the student: ");
	scanf("%s", &address);
	fflush(stdin);
	
	printf("\nPlease enter the email address of the student: ");
	scanf("%s", &email);
	fflush(stdin);
	
	strlwr(email);
	
	printf("     name | roll no. | semester | course | mobile no. | address | email");
	printf("\n %8s %9d %10s %7s %12s %10s %10s", name,roll_no,semester,course,mobile_no, address, email);
	       	
	
	return 0;
	
}
