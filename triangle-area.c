#include <stdio.h>
#include <stdlib.h>


int main(void) {
	double height,base,area;
	printf("Please enter the height of the triangle: ");
	scanf("%Lf", &height);
	printf("\nNow please enter the width of the base of the triangle: ");
	scanf("%Lf", &base);
	area = (base*height)/2;
	printf("\nResultant area of the triangle is: %Lf \n", area);

#ifdef _WIN32
	system("pause");
#endif

	return 0;
}
