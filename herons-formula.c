#include <stdio.h>
#include <math.h>

int main(){
	
	double a,b,c,s,area;
	
	printf("Please enter the 3 sides of the triangle separated by spaces. ");
	scanf("%Lf %Lf %Lf", &a, &b, &c);
	
	s = (a+b+c)/2;
	
	if(s < a || s < b || s < c) {
		printf("\nThe entered values of sides are incorrect\n");
		return 0;
	}
	
	area =  sqrt(s*(s-a)*(s-b)*(s-c));
	
	printf("\nThe area of the triangle is: %Lf\n", area);
	
	return 0;
}
